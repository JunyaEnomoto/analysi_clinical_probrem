from google.colab import drive, auth
from oauth2client.client import GoogleCredentials
import gspread
import pandas as pd
import sys
from sklearn.metrics import roc_curve, roc_auc_score
import matplotlib.pyplot as plt

# オリジナルのライブラリの使用(colabのみ)
sys.path.append("/content/drive/My Drive/細谷グループ/")
drive.mount("/content/drive")


class r_o_c:
    def __init__(self, url=""):
        self.url = url
        pass

    def get_values(self, url, sheet_index) -> gspread.client.Client:
        auth.authenticate_user()
        gc = gspread.authorize(GoogleCredentials.get_application_default())
        sheet = gc.open_by_url(url).get_worksheet(sheet_index)
        df = pd.DataFrame(sheet.get_all_records())
        return df

    def analyse(self, y_true, y_score) -> float:
        """[summary]
        ①ROC曲線を描き、 ② roc areaを出力する

        Parameters
        ----------
        y_true : [list]
            0が false  1が true のlist。
                strや欠損があったらNG
        y_score : [list]
            普通は検査値 -> float が入ったlist

        試してないがpd.seriesもダメと思う。
        """
        fpr, tpr, thresholds = roc_curve(y_true, y_score)  # , drop_intermediate=False)
        plt.plot(fpr, tpr, marker="o")
        plt.xlabel("FPR: False positive rate")
        plt.ylabel("TPR: True positive rate")
        plt.grid()
        print(roc_auc_score(y_true, y_score))

    def wbc_analyse(self):
        """
        # 以下をメインのところに入れて、このクラスを呼び出す。
        if __name__ =="__main__":
            url ="https://docs.google.com/spreadsheets/d/1Za1l_jQnV6H8S6gr8QesA3o9FRSCGMxPrCUCRjYVX8Y/edit#gid=0"
            gunma_konwa=r_o_c(url)
            gunma_konwa.wbc_analyse()
        """
        sheet_index = 3  # analyseのところ。シート動かすとずれるので注意。なお欠損値の関係で df分けているが本来は分けなくてもよい
        wbc_df = self.get_values(self.url, sheet_index)
        y_true = wbc_df["positive"].tolist()
        y_score = wbc_df["wbc"].tolist()
        self.analyse(y_true, y_score)

    def crp_analyse(self):
        """
        wbcと同様
        """
        sheet_index = 4  # analyseのところ。シート動かすとずれるので注意。
        wbc_df = self.get_values(url, sheet_index)
        y_true = wbc_df["positive"].tolist()
        y_score = wbc_df["crp"].tolist()
        self.analyse(y_true, y_score)

    def bt_analyse(self):
        """
        wbcと同様
        """
        sheet_index = 5  # analyseのところ。シート動かすとずれるので注意。
        wbc_df = self.get_values(url, sheet_index)
        y_true = wbc_df["positive"].tolist()
        y_score = wbc_df["BT"].tolist()
        self.analyse(y_true, y_score)
