import pandas as pd
import numpy as np
from scipy.stats import chi2_contingency


def chi2_p(list) -> float:
    """
    listは２次元配列

    """
    obs = np.array(list)
    p = chi2_contingency(obs, correction=False)[1]
    print(p)
    return p


if __name__ == "__main__":
    p_treat_on_the_day = chi2_p([[23, 7], [51, 39]])  # -> p= 0.051 即日治療に対して有意差なし！
    p_crp = chi2_p([[20, 65], [10, 25]])  # -> p=0.56　CRP 1.0で切っても何の意味もない
    p_wbc = chi2_p([[13, 28], [17, 62]])  # -> p=0.22　wbc 9000で切っても何の意味もない
    p_bt = chi2_p([[19, 50], [11, 40]])  # -> p=0.45　bt 37.5 で切っても何の意味もない
